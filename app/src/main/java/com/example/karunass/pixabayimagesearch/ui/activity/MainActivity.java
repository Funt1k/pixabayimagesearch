package com.example.karunass.pixabayimagesearch.ui.activity;

import android.arch.lifecycle.ViewModelProviders;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.example.karunass.pixabayimagesearch.R;
import com.example.karunass.pixabayimagesearch.databinding.ActivityMainBinding;
import com.example.karunass.pixabayimagesearch.ui.adapter.ImagesAdapter;
import com.example.karunass.pixabayimagesearch.viewmodel.SearchImagesViewModel;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements SearchView.OnQueryTextListener {

    private static final int COLUMN_COUNT = 3;

    private ImagesAdapter mImagesAdapter;
    private SearchImagesViewModel mViewModel;

    private int visibleThreshold = 9;
    private int totalItemCount;
    private int[] lastVisibleItems;
    private boolean mIsLoading = false;
    ActivityMainBinding mBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);

        mImagesAdapter = new ImagesAdapter();

        StaggeredGridLayoutManager staggeredGridLayoutManager =
                new StaggeredGridLayoutManager(COLUMN_COUNT, StaggeredGridLayoutManager.VERTICAL);
        mBinding.imageList.setLayoutManager(staggeredGridLayoutManager);

        mBinding.imageList.setAdapter(mImagesAdapter);

        mViewModel = ViewModelProviders.of(this).get(SearchImagesViewModel.class);

        observeImages(mViewModel);

        mBinding.imageList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = staggeredGridLayoutManager.getItemCount();
                lastVisibleItems = staggeredGridLayoutManager.findLastVisibleItemPositions(null);
                if (lastVisibleItems.length > 0) {
                    int maxVisibleItem = maxFromIntArray(lastVisibleItems);
                    if (!mIsLoading && totalItemCount <= (maxVisibleItem + visibleThreshold)) {
                        mViewModel.fetchNextResultsPage();
                        mIsLoading = true;
                    }
                }
            }
        });
    }

    private int maxFromIntArray(int[] lastVisibleItems) {
        Arrays.sort(lastVisibleItems);

        return lastVisibleItems[lastVisibleItems.length - 1];
    }

    private void observeImages(SearchImagesViewModel viewModel) {
        viewModel.getFoundImages().observe(this, hits -> {
            mIsLoading = false;

            showNoImagesMessage(hits == null || hits.isEmpty());
            mImagesAdapter.setImagesList(hits);
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);

        final MenuItem searchItem = menu.findItem(R.id.action_search);
        final SearchView searchView = (SearchView) searchItem.getActionView();
        searchView.setOnQueryTextListener(this);

        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        mViewModel.setLastSearchString(query);

        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        return false;
    }

    private void showNoImagesMessage(boolean needToShow) {
        if (needToShow) {
            mBinding.imageList.setVisibility(View.GONE);
            mBinding.emptyResultMessage.setVisibility(View.VISIBLE);
        } else {
            mBinding.imageList.setVisibility(View.VISIBLE);
            mBinding.emptyResultMessage.setVisibility(View.GONE);
        }
    }
}
