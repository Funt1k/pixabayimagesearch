package com.example.karunass.pixabayimagesearch.ui.adapter;

import android.databinding.DataBindingUtil;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.example.karunass.pixabayimagesearch.R;
import com.example.karunass.pixabayimagesearch.app.GlideApp;
import com.example.karunass.pixabayimagesearch.databinding.ImageItemLayoutBinding;
import com.example.karunass.pixabayimagesearch.service.model.Hit;

import java.util.ArrayList;
import java.util.List;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.ImageViewHolder> {

    private List<Hit> mImagesList = new ArrayList<>();

    public void setImagesList(final List<Hit> imagesList) {
        mImagesList = imagesList;

        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public ImageViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ImageItemLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(parent.getContext()),
                R.layout.image_item_layout, parent, false);

        return new ImageViewHolder(binding);
    }

    @Override
    public int getItemCount() {
        return mImagesList.size();
    }

    @Override
    public void onBindViewHolder(@NonNull ImageViewHolder holder, int position) {
        GlideApp.with(holder.imageBinding.imageView)
                .load(mImagesList.get(position).getPreviewURL())
                .fitCenter()
                .placeholder(R.drawable.no_image_to_show)
                .into(holder.imageBinding.imageView);
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }

    class ImageViewHolder extends RecyclerView.ViewHolder {

        private final ImageItemLayoutBinding imageBinding;

        ImageViewHolder(ImageItemLayoutBinding itemBinding) {
            super(itemBinding.getRoot());

            imageBinding = itemBinding;
        }
    }
}
