package com.example.karunass.pixabayimagesearch.service.repository;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.example.karunass.pixabayimagesearch.service.model.ImageSearchResult;
import com.example.karunass.pixabayimagesearch.utils.Constants;

import java.io.IOException;

import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ImageRepository {
    private ImageSearchService imageSearchService;
    private static ImageRepository imageRepository;
    private MutableLiveData<ImageSearchResult> data = new MutableLiveData<>();

    private ImageRepository() {
        OkHttpClient.Builder httpClient =
                new OkHttpClient.Builder();

        HttpLoggingInterceptor loggingInterceptor = new HttpLoggingInterceptor();
        loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        httpClient.addInterceptor(loggingInterceptor);

        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                HttpUrl originalHttpUrl = original.url();

                HttpUrl url = originalHttpUrl.newBuilder()
                        .addQueryParameter(Constants.KEY, Constants.API_KEY)
                        .addQueryParameter(Constants.PER_PAGE, String.valueOf(Constants.IMAGES_PER_PAGE))
                        .build();

                Request request = original.newBuilder()
                        .url(url).build();
                return chain.proceed(request);
            }
        });

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.API_URL)
                .client(httpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        imageSearchService = retrofit.create(ImageSearchService.class);
    }

    public synchronized static ImageRepository getInstance() {
        if (imageRepository == null) {
            imageRepository = new ImageRepository();
        }

        return imageRepository;
    }

    public MutableLiveData<ImageSearchResult> getImages(String searchQuery) {
        imageSearchService.getImages(searchQuery).enqueue(new Callback<ImageSearchResult>() {
            @Override
            public void onResponse(@NonNull Call<ImageSearchResult> call, @NonNull Response<ImageSearchResult> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<ImageSearchResult> call, @NonNull Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }

    public MutableLiveData<ImageSearchResult> getImagesByPage(String searchQuery, int page) {
        imageSearchService.getImages(searchQuery, page).enqueue(new Callback<ImageSearchResult>() {
            @Override
            public void onResponse(@NonNull Call<ImageSearchResult> call, @NonNull Response<ImageSearchResult> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(@NonNull Call<ImageSearchResult> call, @NonNull Throwable t) {
                data.setValue(null);
            }
        });

        return data;
    }
}
