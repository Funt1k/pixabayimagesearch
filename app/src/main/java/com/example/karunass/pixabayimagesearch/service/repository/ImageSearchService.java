package com.example.karunass.pixabayimagesearch.service.repository;

import com.example.karunass.pixabayimagesearch.service.model.ImageSearchResult;
import com.example.karunass.pixabayimagesearch.utils.Constants;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ImageSearchService {
    @GET("api")
    Call<ImageSearchResult> getImages(@Query(Constants.QUERY) String queryString);

    @GET("api")
    Call<ImageSearchResult> getImages(@Query(Constants.QUERY) String queryString,
                                      @Query(Constants.PAGE) int page);
}
