package com.example.karunass.pixabayimagesearch.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.Transformations;
import android.support.annotation.NonNull;

import com.example.karunass.pixabayimagesearch.service.model.Hit;
import com.example.karunass.pixabayimagesearch.service.model.ImageSearchResult;
import com.example.karunass.pixabayimagesearch.service.repository.ImageRepository;
import com.example.karunass.pixabayimagesearch.utils.Constants;

import java.util.List;

public class SearchImagesViewModel extends AndroidViewModel {

    private MutableLiveData<String> mLastSearchString = new MutableLiveData<>();
    private int mLastSearchCount = 0;
    private int mCurrentResultPage = 1;

    private LiveData<ImageSearchResult> mSearchResult = Transformations.switchMap(mLastSearchString,
            (searchString) -> ImageRepository.getInstance().getImages(searchString));

    private LiveData<List<Hit>> mFoundImages = Transformations.switchMap(mSearchResult, (searchResult)-> {
        List<Hit> currentHits = SearchImagesViewModel.this.mFoundImages.getValue();

        MutableLiveData<List<Hit>> result = new MutableLiveData<>();

        if (searchResult != null) {
            mLastSearchCount = searchResult.getTotal();

            if (mCurrentResultPage > 1) {
                if (currentHits != null) {
                    currentHits.addAll(searchResult.getHits());
                }

                result.setValue(currentHits);
            } else {
                result.setValue(searchResult.getHits());
            }
        } else {
            result.setValue(currentHits);
        }

        return result;
    });

    public SearchImagesViewModel(@NonNull Application application) {
        super(application);
    }

    public void setLastSearchString(String lastSearchString) {
        if (!lastSearchString.equals(mLastSearchString.getValue())) {
            mLastSearchString.setValue(lastSearchString);
            mCurrentResultPage = 1;
        }
    }

    public LiveData<List<Hit>> getFoundImages() {
        return mFoundImages;
    }

    public void fetchNextResultsPage() {
        if (mCurrentResultPage * Constants.IMAGES_PER_PAGE < mLastSearchCount) {
            ++mCurrentResultPage;

            mSearchResult = ImageRepository.getInstance().getImagesByPage(mLastSearchString.getValue(),
                    mCurrentResultPage);
        }
    }

}
