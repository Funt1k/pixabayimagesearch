package com.example.karunass.pixabayimagesearch.utils;

public class Constants {
    public static final String API_URL = "https://pixabay.com/";
    public static final String API_KEY = "9130793-fcde72c2cf62bf2d4747cc3ae";

    public static final int IMAGES_PER_PAGE = 30;

    // Parameteres names
    public static final String KEY = "key";
    public static final String PER_PAGE = "per_page";
    public static final String QUERY = "q";
    public static final String PAGE = "page";
}
